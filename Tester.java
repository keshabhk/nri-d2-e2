package assignment2;

public class Tester {
	public static void main(String args[])
	{
		//create an object of contract employee
		ContractEmployee ce = new ContractEmployee(101, "Keshabh", 1000, 8);
		ce.calculateSalary();
		System.out.println("--CONTRACT EMPLOYEE--");
		System.out.println("ID: "+ce.getEmployeeid());
		System.out.println("ID: "+ce.getEmployeeName());
		System.out.println("ID: "+ce.getSalary());
		
		//create an object of contract employee
		PermanentEmployee pe = new PermanentEmployee(201, "Shubham", 80000, 30000,5);
		pe.calculateMonthlySalary();
		System.out.println("\n--PERMANENT EMPLOYEE--");
		System.out.println("ID: "+pe.getEmployeeid());
		System.out.println("ID: "+pe.getEmployeeName());
		System.out.println("ID: "+pe.getSalary());
		
				
	}
}
