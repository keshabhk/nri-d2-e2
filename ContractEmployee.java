package assignment2;

public class ContractEmployee extends Employee{
  private double wage;
  private float hoursWorked;
  
  public ContractEmployee(int id, String name, double wage, float hoursWorked)
  {
	  super(id,name);
	  this.wage = wage;
	  this.hoursWorked = hoursWorked;
  }

	public double getWage() {
		return wage;
	}
	
	public void setWage(double wage) {
		this.wage = wage;
	}
	
	public float getHoursWorked() {
		return hoursWorked;
	}
	
	public void setHoursWorked(float hoursWorked) {
		this.hoursWorked = hoursWorked;
	}
	  
    
	public void calculateSalary()
	{
		this.setSalary(this.hoursWorked * this.wage);
	}

}
