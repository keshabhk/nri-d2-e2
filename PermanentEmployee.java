package assignment2;

public class PermanentEmployee extends Employee{
	private double basicPay;
	private double hra;
	private float experience;
	
	public PermanentEmployee(int id, String name, double basicPay, double hra, float experience)
	{
		super(id,name);
		this.basicPay = basicPay;
		this.hra = hra;
		this.experience = experience;
	}

	public double getBasicPay() {
		return basicPay;
	}

	public void setBasicPay(double basicPay) {
		this.basicPay = basicPay;
	}

	public double getHra() {
		return hra;
	}

	public void setHra(double hra) {
		this.hra = hra;
	}

	public float getExperience() {
		return experience;
	}

	public void setExperience(float experience) {
		this.experience = experience;
	}
	
	public void calculateMonthlySalary()
	{
		double component = 0;
		if(this.experience<3)
			component = 0;
		else if(this.experience>=3 && this.experience<5)
			component = 5;
		else if(this.experience>=5 && this.experience<10)
			component = 7;
		else if(this.experience>=10)
			component = 12;
		this.setSalary(Math.round(this.basicPay + this.hra + component));
	}

}
