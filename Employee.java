package assignment2;

public class Employee {
	private int employeeid;
	private String employeeName;
	private double salary;
	
	public Employee(int employeeid, String employeeName)
	{
		this.employeeid = employeeid;
		this.employeeName = employeeName;
	}

	public int getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
}
